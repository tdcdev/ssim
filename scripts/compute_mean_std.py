#!/usr/bin/env python


import os
import sys
import csv
import numpy as np


def error(msg):
    sys.stderr.write('ERROR: ' + msg + '\n')


def to_mean_std(arg):
    try:
        parser = csv.reader(open(arg), delimiter=' ')
        iterations = []
        for line in parser: 
            for i, n in enumerate([n for n in line if n]):
                if i < len(iterations):
                    iterations[i].append(float(n))
                else:
                    iterations.append([float(n)])
        try:
            name = arg
            if name.endswith('.dat'):
                name = name[:-4]
            name = name + '_mean_std.dat'
            with open(name, 'w') as f:
                for i, it in enumerate(iterations):
                    mean = np.mean(np.array(it))
                    std = np.std(np.array(it))
                    f.write(str(i + 1) + ' ' + str(mean) + ' ' + str(std) + ' ')
                    f.write(str(mean - std) + ' ' + str(mean + std) + '\n')
        except:
            error('unable to write \'' + name + '\'')
    except:
        error('unable to parse \'' + arg + '\'')


if __name__ == '__main__':
    for arg in sys.argv[1:]:
        if os.path.isfile(arg):
            to_mean_std(arg)
        else:
            error('\'' + arg + '\' does not exist')
