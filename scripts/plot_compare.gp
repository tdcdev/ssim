dat1 = 'gradientr_mean_std.dat'
dat2 = 'hillc_mean_std.dat'

set term pdf enhanced
set output 'compare.pdf'

set key bottom right
set xlabel "iterations"
set ylabel "fitness"
set xtics nomirror
set ytics nomirror
set xrange [0:100]
set yrange [-1:25]
set style fill transparent solid 0.30 noborder

plot \
dat1 using 1:4:5 w filledcurves lc rgb "green" t "std gradient", \
dat2 using 1:4:5 w filledcurves lc rgb "orange" t "std hillc", \
dat1 using 1:2 w l lc rgb "red" t "mean gradient", \
dat2 using 1:2 w l lc rgb "blue" t "mean hillc"
