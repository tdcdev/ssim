if (!exists("dat")) print "no 'dat' variable set"; exit
if (!exists("out")) print "no 'out' variable set"; exit

set terminal png
set output out

set key bottom right
set xlabel "iterations"
set ylabel "fitness"
set xtics nomirror
set ytics nomirror
set xrange [0:100]
set yrange [-1:25]
set style fill transparent solid 0.30 noborder

plot \
dat using 1:4:5 w filledcurves lc rgb "green" t "standard deviation", \
dat using 1:2 w l lc rgb "red" t "mean" 
