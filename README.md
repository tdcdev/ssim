SSIM
====

Requirements
------------

- SFML 2.1
- Bullet Physics 3.x
- Boost 1.55.x
- CMake 2.8.9
- g++ 4.8

Build
-----

To download this repository use this command line:

    git clone --recursive https://bitbucket.org/tdcdev/ssim

Once the requirements have been installed, use the following commands in order
to compile:

    mkdir build
    cd build
    cmake ..
    make
    make install

If one or several requirements are not install in a standard location you can
define the paths with environment variables like this:

    BOOST_ROOT=<path-to-boost> \
    SFML_ROOT=<path-to-sfml> \
    BULLET_ROOT=<path-to-bullet> \
    cmake ..

Note that you have to install the project to get it work.  If you want install
it in local path you can use cmake like this:

    cmake -DCMAKE_INSTALL_PREFIX=<path-to-install> ..

Usage
-----

There are two executables: `ssim` and `comparator`.

The first one open a window and allows you to see the current robot learn to
walk.

    ./install/prefix/you/defined/ssim

Or if installed in standard path:

    ssim

The second one run a number of learning algorithms and save data.

    ./install/prefix/you/defined/comparator

Or if installed in standard path:

    comparator

Directories list
----------------

- include: project headers
- src: project sources files
- res: resources (images, fonts, etc...)
- scripts: scripts to processing data from `comparator`
- results: data and plots
- 3rdparty: third-party library for xml parsing
- cmake: CMake macros
- config: headers for configuration
