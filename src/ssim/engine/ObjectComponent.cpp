#include <ssim/engine/ObjectComponent.hpp>



ObjectComponent::ObjectComponent(const std::string& id):
    m_id(id)
{

}



ObjectComponent::~ObjectComponent()
{

}



const std::string& ObjectComponent::id() const
{
    return m_id;
}
