#include <ssim/engine/Object.hpp>
#include <btBulletDynamicsCommon.h>



Object::Object():
    m_time(0.f)
{
    m_broadphase.reset(new btDbvtBroadphase());
    m_colConfig.reset(new btDefaultCollisionConfiguration());
    m_dispatcher.reset(new btCollisionDispatcher(m_colConfig.get()));
    m_solver.reset(new btSequentialImpulseConstraintSolver());
    m_groundShape.reset(new btStaticPlaneShape(btVector3(0.f, 1.f, 0.f), 0.f));
    m_groundMotionState.reset(new btDefaultMotionState());
    btRigidBody::btRigidBodyConstructionInfo infos(
            0,
            m_groundMotionState.get(),
            m_groundShape.get(),
            btVector3(0.f, 0.f, 0.f)
            );
    m_groundRigidBody.reset(new btRigidBody(infos));
    m_world.reset(new
            btDiscreteDynamicsWorld(
                m_dispatcher.get(),
                m_broadphase.get(),
                m_solver.get(),
                m_colConfig.get()
                )
            );

    m_world->setGravity(btVector3(0.f, -10.f, 0.f));
    m_world->addRigidBody(m_groundRigidBody.get());
}



Object::~Object()
{
    m_world->removeRigidBody(m_groundRigidBody.get());
}



void Object::stepSimulation(float seconds)
{
    m_time += seconds;

    while (m_time > 0.03f)
    {
        float remaining = 0.03f;

        m_time -= 0.03f;

        while (remaining > 0.f)
        {
            float delta = remaining < 0.01f ? remaining : 0.01f;

            this->simulate(delta);
            m_world->stepSimulation(delta);
            remaining -= delta;
        }
    }
}
