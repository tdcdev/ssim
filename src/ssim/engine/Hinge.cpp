#include <ssim/engine/Hinge.hpp>
#include <ssim/engine/RigidBody.hpp>
#include <btBulletDynamicsCommon.h>



Hinge::Hinge(
        const std::string& id,
        std::shared_ptr<RigidBody> rigidBodyA,
        std::shared_ptr<RigidBody> rigidBodyB,
        btVector3 pivA,
        btVector3 pivB,
        btVector3 axis,
        float min,
        float max
        ):
    ObjectComponent(id),
    m_rigidBodyA(rigidBodyA),
    m_rigidBodyB(rigidBodyB)
{
    m_hingeConstraint.reset(
            new btHingeConstraint(
                *m_rigidBodyA->m_rigidBody,
                *m_rigidBodyB->m_rigidBody,
                pivA,
                pivB,
                axis,
                axis,
                false
                )
            );
    m_hingeConstraint->setLimit(min, max, 0.9, 0.1f, 0.f);
}



Hinge::~Hinge()
{

}



float Hinge::min() const
{
    return m_hingeConstraint->getLowerLimit();
}



float Hinge::max() const
{
    return m_hingeConstraint->getUpperLimit();
}



float Hinge::angle() const
{
    return m_hingeConstraint->getHingeAngle();
}



void Hinge::setSpeed(float speed, float impulse)
{
    m_rigidBodyA->activate();
    m_rigidBodyB->activate();
    m_hingeConstraint->enableAngularMotor(true, speed, impulse);
}



void Hinge::addToWorld(btDiscreteDynamicsWorld* world)
{
    world->addConstraint(m_hingeConstraint.get(), true);
}



void Hinge::removeFromWorld(btDiscreteDynamicsWorld* world)
{
    world->removeConstraint(m_hingeConstraint.get());
}
