#include <ssim/engine/Robot.hpp>
#include <ssim/engine/RigidBody.hpp>
#include <ssim/engine/Hinge.hpp>
#include <tinyxml2/tinyxml2.h>
#include <btBulletDynamicsCommon.h>
#include <iostream>



Robot::Robot():
    m_progress(0.f)
{

}



Robot::~Robot()
{
    for(RigidBodyMap::iterator i = m_bodies.begin(); i != m_bodies.end(); i++)
    {
        i->second->removeFromWorld(m_world.get());
    }

    for(HingeMap::iterator i = m_hinges.begin(); i != m_hinges.end(); i++)
    {
        i->second->removeFromWorld(m_world.get());
    }
}



const RigidBodyMap& Robot::bodies() const
{
    return m_bodies;
}



const HingeMap& Robot::hinges() const
{
    return m_hinges;
}



const std::shared_ptr<RigidBody> Robot::body(const std::string& id) const
{
    RigidBodyMap::const_iterator i = m_bodies.find(id);

    if (i != m_bodies.end())
    {
        return i->second;
    }

    return nullptr;
}



const std::shared_ptr<Hinge> Robot::hinge(const std::string& id) const
{
    HingeMap::const_iterator i = m_hinges.find(id);

    if (i != m_hinges.end())
    {
        return i->second;
    }

    return nullptr;
}



const std::vector<float>& Robot::policy() const
{
    return m_policy;
}



bool Robot::load(const std::string& file)
{
    using namespace tinyxml2;

    XMLDocument doc;

    if (doc.LoadFile(file.c_str()) != XML_NO_ERROR)
    {
        return false;
    }

    XMLNode* root = doc.FirstChildElement("model");

    if (root == 0)
    {
        return false;
    }

    XMLNode* bodies = root->FirstChildElement("bodies");
    XMLElement* body = bodies->FirstChildElement("body");

    while (body)
    {
        const char* tags[8] = {"id", "x", "y", "z", "ox", "oy", "oz", "mass"};
        float values[8];
        std::string id;

        for (int i = 0; i < 8; i++)
        {
            if (!body->Attribute(tags[i]))
            {
                std::cerr << "Error loading body: no " << tags[i] << std::endl;
                return false;
            }

            body->QueryFloatAttribute(tags[i], values + i);
        }

        id = body->Attribute(tags[0]);

        m_bodies[id] = std::shared_ptr<RigidBody>(
                new RigidBody(
                    id,
                    btVector3(values[1], values[2], values[3]),
                    btVector3(values[4], values[5], values[6]),
                    values[7]
                    )
                );

        body = body->NextSiblingElement("body");
    }

    XMLNode* hinges = root->FirstChildElement("hinges");
    XMLElement* hinge = hinges->FirstChildElement("hinge");

    while (hinge)
    {
        const char* tags[] = {
            "id", "ida", "idb",
            "pax", "pay", "paz", "pbx", "pby", "pbz",
            "axisx", "axisy", "axisz", "min", "max"
        };
        float values[14];
        std::string id, ida, idb;

        for (int i = 0; i < 14; i++)
        {
            if (!hinge->Attribute(tags[i]))
            {
                std::cerr << "Error loading hinge: no " << tags[i] << std::endl;
                return false;
            }

            hinge->QueryFloatAttribute(tags[i], values + i);
        }

        id = hinge->Attribute(tags[0]);
        ida = hinge->Attribute(tags[1]);
        idb = hinge->Attribute(tags[2]);

        RigidBodyMap::iterator a = m_bodies.find(ida);
        RigidBodyMap::iterator b = m_bodies.find(idb);

        if (a == m_bodies.end())
        {
            std::cerr << "Error loading hinge: no body " << ida << std::endl;
            return false;
        }

        if (b == m_bodies.end())
        {
            std::cerr << "Error loading hinge: no body " << idb << std::endl;
            return false;
        }

        m_hinges[id] = std::shared_ptr<Hinge>(
                new Hinge(
                    id,
                    a->second,
                    b->second,
                    btVector3(values[3], values[4], values[5]),
                    btVector3(values[6], values[7], values[8]),
                    btVector3(values[9], values[10], values[11]),
                    values[12],
                    values[13]
                    )
                );

        hinge = hinge->NextSiblingElement("hinge");
    }

    for(RigidBodyMap::iterator i = m_bodies.begin(); i != m_bodies.end(); i++)
    {
        i->second->addToWorld(m_world.get());
    }

    for(HingeMap::iterator i = m_hinges.begin(); i != m_hinges.end(); i++)
    {
        i->second->addToWorld(m_world.get());
    }

    return true;
}



bool Robot::setPolicy(const std::vector<float>& policy)
{
    if (policy.size() != m_hinges.size() * 4)
    {
        std::cerr << "Policy error: wrong parameters" << std::endl;
        return false;
    }

    m_policy = policy;

    return true;
}



void Robot::simulate(float seconds)
{
    float next = m_progress + seconds * M_PI;
    int n = 0;

    while (next > 2 * M_PI)
    {
        next -= 2 * M_PI;
    }

    while (next < 0.f)
    {
        next += 2 * M_PI;
    }

    for (HingeMap::iterator i = m_hinges.begin(); i != m_hinges.end(); i++)
    {
        float a = m_policy[n++];
        float b = m_policy[n++];
        float c = m_policy[n++];
        float d = m_policy[n++];
        float delta = a * sin(b * next + c) + d - i->second->angle();
        i->second->setSpeed(delta / seconds);
    }

    m_progress = next;
}
