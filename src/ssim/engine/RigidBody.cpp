#include <ssim/engine/RigidBody.hpp>
#include <btBulletDynamicsCommon.h>



RigidBody::RigidBody(
        const std::string& id,
        btVector3 size,
        btVector3 origin,
        float mass
        ):
    m_id(id),
    m_size(size),
    m_origin(origin),
    m_mass(mass)
{

    btTransform trans;
    trans.setIdentity();
    trans.setOrigin(m_origin);
    btVector3 inertia(0.0f, 0.0f, 0.0f);
    m_shape.reset(new btBoxShape(m_size));
    m_shape->calculateLocalInertia(m_mass, inertia);
    m_mostionState.reset(new btDefaultMotionState(trans));
    btRigidBody::btRigidBodyConstructionInfo infos(
            m_mass,
            m_mostionState.get(),
            m_shape.get(),
            inertia
            );
    m_rigidBody.reset(new btRigidBody(infos));
}



RigidBody::~RigidBody()
{

}



const std::string& RigidBody::id() const
{
    return m_id;
}



btVector3 RigidBody::size() const
{
    return m_size;
}



btVector3 RigidBody::origin() const
{
    return m_origin;
}



float RigidBody::mass() const
{
    return m_mass;
}



btVector3 RigidBody::centerOfMass() const
{
    return m_rigidBody->getCenterOfMassPosition();
}



void RigidBody::getMatrix(float* matrix) const
{
    btTransform trans;
    m_rigidBody->getMotionState()->getWorldTransform(trans);
    trans.getOpenGLMatrix(matrix);
}



void RigidBody::activate()
{
    m_rigidBody->activate();
}



void RigidBody::addToWorld(btDiscreteDynamicsWorld* world)
{
    world->addRigidBody(m_rigidBody.get());
}



void RigidBody::removeFromWorld(btDiscreteDynamicsWorld* world)
{
    world->removeRigidBody(m_rigidBody.get());
}
