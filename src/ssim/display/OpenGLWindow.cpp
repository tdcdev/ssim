#include <ssim/display/OpenGLWindow.hpp>
#include <ssim/engine/Robot.hpp>
#include <ssim/engine/RigidBody.hpp>
#include <ssim/ssim/tools.hpp>
#include <iostream>



OpenGLWindow::OpenGLWindow(
        unsigned int w,
        unsigned int h,
        const std::string& name
        ):
    sf::RenderWindow(
            sf::VideoMode(w, h),
            name,
            sf::Style::Default,
            OpenGLWindow::generateContextSettings()
            ),
    m_width(w),
    m_height(h),
    m_deltaTime(0.f)
{
    m_camForward[0] = 0.f;
    m_camForward[1] = 0.f;
    m_camForward[2] = 1.f;
    m_camForward[3] = 1.f;
    m_camLeft[0] = 1.f;
    m_camLeft[1] = 0.f;
    m_camLeft[2] = 0.f;
    m_camLeft[3] = 1.f;
    m_camTarget[0] = 0.f;
    m_camTarget[1] = 0.f;
    m_camTarget[2] = 0.f;
    m_camTarget[3] = 1.f;
    m_camDistance = 5.f;
    m_camSpeed = 20.f;
}



OpenGLWindow::~OpenGLWindow()
{

}



float OpenGLWindow::deltaTime() const
{
    return m_deltaTime;
}



void OpenGLWindow::init()
{
    this->setFramerateLimit(60);
    this->setVerticalSyncEnabled(true);
    this->setActive();

    if (!m_texGround.loadFromFile(getResource("ground.png")))
    {
        std::cerr << "Error loading ground texture" << std::endl;
    }

    if (!m_texCube.loadFromFile(getResource("cube.png")))
    {
        std::cerr << "Error loading cube texture" << std::endl;
    }

    m_texGround.setRepeated(true);
    m_texGround.setSmooth(true);
    m_texCube.setRepeated(true);
    m_texCube.setSmooth(true);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);
    glDepthMask(GL_TRUE);
    glClearDepth(1.f);
    glClearColor(0.7, 0.7, 0.8, 0.0);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0, (double)m_width / (double)m_height, 0.001, 1000);
}



void OpenGLWindow::processEvents()
{
    sf::Event event;

    m_deltaTime = m_clock.restart().asSeconds();

    while (this->pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
        {
            this->close();
        }

        if (event.type == sf::Event::KeyPressed)
        {
            if (event.key.code == sf::Keyboard::Escape)
            {
                this->close();
            }
        }

        if (event.type == sf::Event::Resized)
        {
            glViewport(0, 0, event.size.width, event.size.height);
            this->setView(
                    sf::View(
                        sf::FloatRect(0, 0, event.size.width, event.size.height)
                        )
                    );
            m_width = event.size.width;
            m_height = event.size.height;
        }
    }

    this->processCamEvents();
}



void OpenGLWindow::setupCam(float x, float y, float z)
{
    float lightPos[4] = {0.f, 0.f, 0.f, 1.f};
    float camPos[4] = {0.f, 0.f, 0.f, 1.f};

    m_camTarget[0] = x;
    m_camTarget[1] = y;
    m_camTarget[2] = z;

    for (int i = 0; i < 3; i++)
    {
        camPos[i] = m_camTarget[i] + m_camForward[i] * m_camDistance;
    }

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
    gluLookAt(
            camPos[0], camPos[1], camPos[2],
            m_camTarget[0], m_camTarget[1], m_camTarget[2],
            0.f, 1.f, 0.f
            );
}



void OpenGLWindow::enableLighting(bool on)
{
    if (on)
    {
        glEnable(GL_LIGHTING);
    }
    else
    {
        glDisable(GL_LIGHTING);
    }
}



void OpenGLWindow::enableTextures(bool on)
{
    if (on)
    {
        glEnable(GL_TEXTURE_2D);
    }
    else
    {
        glDisable(GL_TEXTURE_2D);
    }
}



void OpenGLWindow::drawMark()
{
    GLfloat mark[] =
    {
        0.f, 0.f, 0.f, 1.f, 0.f, 0.f,
        1.f, 0.f, 0.f, 1.f, 0.f, 0.f,
        0.f, 0.f, 0.f, 0.f, 1.f, 0.f,
        0.f, 1.f, 0.f, 0.f, 1.f, 0.f,
        0.f, 0.f, 0.f, 0.f, 0.f, 1.f,
        0.f, 0.f, 1.f, 0.f, 0.f, 1.f,
    };

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);

    glVertexPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), mark);
    glColorPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), mark + 3);

    glDrawArrays(GL_LINES, 0, 6);

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
}



void OpenGLWindow::drawGround(float side)
{
    GLfloat ground[] =
    {
        -1.f, 0.f, -1.f, 0.f, 1.f, 0.f, 1.f, 1.f, 1.f, 0.f, 0.f,
        1.f, 0.f, -1.f, 0.f, 1.f, 0.f, 1.f, 1.f, 1.f, 2.f, 0.f,
        -1.f, 0.f, 1.f, 0.f, 1.f, 0.f, 1.f, 1.f, 1.f, 0.f, 2.f,
        -1.f, 0.f, 1.f, 0.f, 1.f, 0.f, 1.f, 1.f, 1.f, 0.f, 2.f,
        1.f, 0.f, -1.f, 0.f, 1.f, 0.f, 1.f, 1.f, 1.f, 2.f, 0.f,
        1.f, 0.f, 1.f, 0.f, 1.f, 0.f, 1.f, 1.f, 1.f, 2.f, 2.f,
    };

    for (int i = 0; i < 66; i++)
    {
        ground[i] *= side;
    }

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glVertexPointer(3, GL_FLOAT, 11 * sizeof(GLfloat), ground);
    glNormalPointer(GL_FLOAT, 11 * sizeof(GLfloat), ground + 3);
    glColorPointer(3, GL_FLOAT, 11 * sizeof(GLfloat), ground + 6);
    glTexCoordPointer(2, GL_FLOAT, 11 * sizeof(GLfloat), ground + 9);

    this->enableTextures(true);
    sf::Texture::bind(&m_texGround);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    this->enableTextures(false);

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}



void OpenGLWindow::drawCube(float x, float y, float z)
{
    GLfloat cube[] =
    {
        -1.f, -1.f, -1.f, -1.f, 0.f, 0.f, 1.f, 1.f, 1.f, 0.f, 0.f,
        -1.f, 1.f, -1.f, -1.f, 0.f, 0.f, 1.f, 1.f, 1.f, 1.f, 0.f,
        -1.f, -1.f, 1.f, -1.f, 0.f, 0.f, 1.f, 1.f, 1.f, 0.f, 1.f,
        -1.f, -1.f, 1.f, -1.f, 0.f, 0.f, 1.f, 1.f, 1.f, 0.f, 1.f,
        -1.f, 1.f, -1.f, -1.f, 0.f, 0.f, 1.f, 1.f, 1.f, 1.f, 0.f,
        -1.f, 1.f, 1.f, -1.f, 0.f, 0.f, 1.f, 1.f, 1.f, 1.f, 1.f,

        1.f, -1.f, -1.f, 1.f, 0.f, 0.f, 1.f, 1.f, 1.f, 0.f, 0.f,
        1.f, 1.f, -1.f, 1.f, 0.f, 0.f, 1.f, 1.f, 1.f, 1.f, 0.f,
        1.f, -1.f, 1.f, 1.f, 0.f, 0.f, 1.f, 1.f, 1.f, 0.f, 1.f,
        1.f, -1.f, 1.f, 1.f, 0.f, 0.f, 1.f, 1.f, 1.f, 0.f, 1.f,
        1.f, 1.f, -1.f, 1.f, 0.f, 0.f, 1.f, 1.f, 1.f, 1.f, 0.f,
        1.f, 1.f, 1.f, 1.f, 0.f, 0.f, 1.f, 1.f, 1.f, 1.f, 1.f,

        -1.f, -1.f, -1.f, 0.f, -1.f, 0.f, 1.f, 1.f, 1.f, 0.f, 0.f,
        1.f, -1.f, -1.f, 0.f, -1.f, 0.f, 1.f, 1.f, 1.f, 1.f, 0.f,
        -1.f, -1.f, 1.f, 0.f, -1.f, 0.f, 1.f, 1.f, 1.f, 0.f, 1.f,
        -1.f, -1.f, 1.f, 0.f, -1.f, 0.f, 1.f, 1.f, 1.f, 0.f, 1.f,
        1.f, -1.f, -1.f, 0.f, -1.f, 0.f, 1.f, 1.f, 1.f, 1.f, 0.f,
        1.f, -1.f, 1.f, 0.f, -1.f, 0.f, 1.f, 1.f, 1.f, 1.f, 1.f,

        -1.f, 1.f, -1.f, 0.f, 1.f, 0.f, 1.f, 1.f, 1.f, 0.f, 0.f,
        1.f, 1.f, -1.f, 0.f, 1.f, 0.f, 1.f, 1.f, 1.f, 1.f, 0.f,
        -1.f, 1.f, 1.f, 0.f, 1.f, 0.f, 1.f, 1.f, 1.f, 0.f, 1.f,
        -1.f, 1.f, 1.f, 0.f, 1.f, 0.f, 1.f, 1.f, 1.f, 0.f, 1.f,
        1.f, 1.f, -1.f, 0.f, 1.f, 0.f, 1.f, 1.f, 1.f, 1.f, 0.f,
        1.f, 1.f, 1.f, 0.f, 1.f, 0.f, 1.f, 1.f, 1.f, 1.f, 1.f,

        -1.f, -1.f, -1.f, 0.f, 0.f, -1.f, 1.f, 1.f, 1.f, 0.f, 0.f,
        1.f, -1.f, -1.f, 0.f, 0.f, -1.f, 1.f, 1.f, 1.f, 1.f, 0.f,
        -1.f, 1.f, -1.f, 0.f, 0.f, -1.f, 1.f, 1.f, 1.f, 0.f, 1.f,
        -1.f, 1.f, -1.f, 0.f, 0.f, -1.f, 1.f, 1.f, 1.f, 0.f, 1.f,
        1.f, -1.f, -1.f, 0.f, 0.f, -1.f, 1.f, 1.f, 1.f, 1.f, 0.f,
        1.f, 1.f, -1.f, 0.f, 0.f, -1.f, 1.f, 1.f, 1.f, 1.f, 1.f,

        -1.f, -1.f, 1.f, 0.f, 0.f, 1.f, 1.f, 1.f, 1.f, 0.f, 0.f,
        1.f, -1.f, 1.f, 0.f, 0.f, 1.f, 1.f, 1.f, 1.f, 1.f, 0.f,
        -1.f, 1.f, 1.f, 0.f, 0.f, 1.f, 1.f, 1.f, 1.f, 0.f, 1.f,
        -1.f, 1.f, 1.f, 0.f, 0.f, 1.f, 1.f, 1.f, 1.f, 0.f, 1.f,
        1.f, -1.f, 1.f, 0.f, 0.f, 1.f, 1.f, 1.f, 1.f, 1.f, 0.f,
        1.f, 1.f, 1.f, 0.f, 0.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f,
    };

    glPushMatrix();
    glScalef(x, y, z);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glVertexPointer(3, GL_FLOAT, 11 * sizeof(GLfloat), cube);
    glNormalPointer(GL_FLOAT, 11 * sizeof(GLfloat), cube + 3);
    glColorPointer(3, GL_FLOAT, 11 * sizeof(GLfloat), cube + 6);
    glTexCoordPointer(2, GL_FLOAT, 11 * sizeof(GLfloat), cube + 9);


    this->enableLighting(true);
    this->enableTextures(true);
    sf::Texture::bind(&m_texCube);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    this->enableLighting(false);
    this->enableTextures(false);

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);

    glPopMatrix();
}



void OpenGLWindow::drawRigidBody(const RigidBody& body)
{
    btScalar matrix[16];

    body.getMatrix(matrix);
    glPushMatrix();
    glMultMatrixf(matrix);
    this->drawCube(body.size().x(), body.size().y(), body.size().z());
    glPopMatrix();
}



void OpenGLWindow::drawRobot(const Robot& robot)
{
    const RigidBodyMap& map = robot.bodies();

    for (RigidBodyMap::const_iterator i = map.begin(); i != map.end(); i++)
    {
        this->drawRigidBody(*i->second);
    }
}



sf::ContextSettings OpenGLWindow::generateContextSettings()
{
    sf::ContextSettings settings;

    settings.depthBits = 32;

    return settings;
}



void OpenGLWindow::processCamEvents()
{
    float up[3] = {0.f, 1.f, 0.f};
    float rad = m_deltaTime * m_camSpeed * 3 / 180.f * M_PI;

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Q))
    {
        rotate(m_camForward, up, rad);
        rotate(m_camLeft, up, rad);
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D))
    {
        rotate(m_camForward, up, -rad);
        rotate(m_camLeft, up, -rad);
    }

    normalize(m_camLeft);

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::R))
    {
        rotate(m_camForward, m_camLeft, rad);
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::F))
    {
        rotate(m_camForward, m_camLeft, -rad);
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Z))
    {
        m_camDistance -= m_deltaTime * m_camSpeed;
        m_camDistance = m_camDistance < 1.f ? 1.f : m_camDistance;
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S))
    {
        m_camDistance += m_deltaTime * m_camSpeed;
    }

    normalize(m_camForward);
}
