#include <ssim/ssim/tools.hpp>
#include <ssim/version.h>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>



boost::filesystem::path g_path;
std::random_device g_rd;
std::mt19937 g_gen(g_rd());



bool initResourcePath()
{
    g_path = boost::filesystem::system_complete(
            boost::filesystem::path(RESOURCES_DIR)
            );

    if (!boost::filesystem::exists(g_path))
    {
        g_path.clear();
        return false;
    }

    return true;
}



std::string getResource(const std::string& res)
{
    boost::filesystem::path tmp(g_path/res);

    if (!boost::filesystem::exists(tmp))
    {
        return std::string(res);
    }

    return std::string(tmp.string());
}



int rand(int min, int max)
{
    return std::uniform_int_distribution<>(min, max)(g_gen);
}



double randReal()
{
  return std::uniform_real_distribution<>()(g_gen);
}



void shuffleIntVector(std::vector<int>& v)
{
    std::shuffle(v.begin(), v.end(), g_gen);
}



void rotate(float* res, float* v, float* axis, float rad)
{
    float cosinus = cos(rad);
    float sinus = sin(rad);
    float x = axis[0];
    float y = axis[1];
    float z = axis[2];
    float mat[3][3] = {
        {
            x * x * (1 - cosinus) + cosinus,
            x * y * (1 - cosinus) - z * sinus,
            x * z * (1 - cosinus) + y * sinus
        },
        {
            y * x * (1 - cosinus) + z * sinus,
            y * y * (1 - cosinus) + cosinus,
            y * z * (1 - cosinus) - x * sinus
        },
        {
            z * x * (1 - cosinus) - y * sinus,
            z * y * (1 - cosinus) + x * sinus,
            z * z * (1 - cosinus) + cosinus
        }
    };

    for(int i = 0; i < 3; i++)
    {
        res[i] = v[0] * mat[0][i] + v[1] * mat[1][i] + v[2] * mat[2][i];
    }
}



void rotate(float* v, float* axis, float rad)
{
    float res[3];

    rotate(res, v, axis, rad);

    for (int i = 0; i < 3; i++)
    {
        v[i] = res[i];
    }
}



float norm(const float* v)
{
    float norm = 0.f;

    for (int i = 0; i < 3; i++)
    {
        norm += v[i] * v[i];
    }

    return sqrt(norm);
}



void normalize(float* v)
{
    float n = norm(v);

    if (std::fabs(n) > std::numeric_limits<float>::epsilon())
    {
        for (int i = 0; i < 3; i++)
        {
            v[i] /= n;
        }
    }
}



void normalize(std::vector<float>& v)
{
    float acc = 0.f;

    for (std::vector<float>::iterator i = v.begin(); i != v.end(); i++)
    {
        acc += (*i * *i);
    }

    acc = sqrt(acc);

    if (std::fabs(acc) > std::numeric_limits<float>::epsilon())
    {
        for (std::vector<float>::iterator i = v.begin(); i != v.end(); i++)
        {
            *i /= acc;
        }
    }
}



float mean(const std::vector<float>& v)
{
    float acc = 0.f;

    if (v.empty())
    {
        return acc;
    }

    for (std::vector<float>::const_iterator i = v.begin(); i != v.end(); i++)
    {
        acc += *i;
    }

    return acc / (float)v.size();
}
