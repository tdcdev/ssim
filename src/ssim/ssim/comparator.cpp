#include <ssim/engine/Robot.hpp>
#include <ssim/engine/RigidBody.hpp>
#include <ssim/algorithm/GradientR.hpp>
#include <ssim/algorithm/HillC.hpp>
#include <ssim/algorithm/Annealing.hpp>
#include <ssim/ssim/fitness.hpp>
#include <ssim/ssim/tools.hpp>
#include <map>
#include <iostream>
#include <fstream>



typedef std::shared_ptr<Algorithm> AlgoPtr;
typedef std::map<std::string, AlgoPtr> AlgoMap;
unsigned int g_nbTrails = 50;
unsigned int g_nbIterations = 100;



int main(int argc, char** argv)
{
    initResourcePath();

    std::map<std::string, AlgoPtr> algos;

    algos["gradientr"] = AlgoPtr(
            new GradientR(&fitness, 30, std::vector<float>(16, 0.1f), 0.9f)
            );

    algos["hillc"] = AlgoPtr(
            new HillC(&fitness, 30, std::vector<float>(16, 0.1f))
            );

    for (
            AlgoMap::iterator algoit = algos.begin();
            algoit != algos.end();
            algoit++
        )
    {
        AlgoPtr algo(algoit->second);
        std::ofstream file(algoit->first + ".dat");

        std::cout << "# " << algoit->first << std::endl;

        for (unsigned int i = 0; i < g_nbTrails; i++)
        {
            algo->setStartingPoint(std::vector<float>(16, 0.f));
            algo->run(g_nbIterations);
            
            std::vector<float> evolution(algo->fitnessEvolution());
            
            for (
                std::vector<float>::const_iterator f = evolution.begin();
                f != evolution.end();
                f++
                )
            {
                std::cout << *f << " ";
                file << *f << " ";
            }
            
            std::cout << std::endl;
            file << std::endl;
        }

        file.close();
    }


    return 0;
}
