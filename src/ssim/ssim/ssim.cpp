#include <ssim/display/OpenGLWindow.hpp>
#include <ssim/engine/Robot.hpp>
#include <ssim/engine/RigidBody.hpp>
#include <ssim/algorithm/GradientR.hpp>
#include <ssim/ssim/fitness.hpp>
#include <ssim/ssim/tools.hpp>
#include <memory>
#include <thread>
#include <atomic>
#include <iostream>



std::atomic<bool> g_lock(false);
sf::Font g_font;



void drawStats(sf::RenderWindow& window, const Algorithm& algo)
{
    float winWidth = window.getSize().x;
    float graphWidth = 200.f;
    float graphHeight = 150.f;
    float gap = 30.f;
    sf::Color shapeColor = sf::Color(0, 0, 0, 150);

    sf::Text text;
    sf::RectangleShape shape;
    sf::RectangleShape graph;

    text.setFont(g_font);
    text.setString(
            "fitness:    \t" + std::to_string(algo.bestFitness()) + "\n" +
            "iterations: \t" + std::to_string(algo.iterationCalls())
            );
    text.setCharacterSize(20);
    shape.setFillColor(shapeColor);
    shape.setSize(
            sf::Vector2f(
                text.getLocalBounds().width + gap, 
                text.getLocalBounds().height + gap
                )
            );
    text.setPosition(gap / 2.f, gap / 3.f);
    graph.setFillColor(shapeColor);
    graph.setSize(sf::Vector2f(graphWidth, graphHeight));
    graph.setPosition(winWidth - graphWidth, 0.f);

    sf::VertexArray zero(sf::Lines);
    sf::VertexArray lines(sf::LinesStrip);
    const std::vector<float>& evo(algo.fitnessEvolution());
    float ox = winWidth - graphWidth;
    float oy = graphHeight;
    float max = algo.bestFitness();
    float min = 0.f;

    for (unsigned int i = 0; i < evo.size(); i++)
    {
        if (evo[i] < min)
        {
            min = evo[i];
        }
    }

    zero.append(
            sf::Vertex(sf::Vector2f(ox, oy + min), sf::Color::Blue)
            );
    zero.append(
            sf::Vertex(sf::Vector2f(ox + graphWidth, oy + min), sf::Color::Blue)
            );

    for (unsigned int i = 0; i < evo.size(); i++)
    {
        float x = (float)i / (float)evo.size() * graphWidth;
        float y = (evo[i] - min) / (max - min) * graphHeight;

        lines.append(sf::Vertex(sf::Vector2f(ox + x, oy - y), sf::Color::Red));
    }

    window.pushGLStates();
    window.draw(shape);
    window.draw(text);
    window.draw(graph);
    window.draw(zero);
    window.draw(lines);
    window.popGLStates();
}



void drawWaitingFrame(sf::RenderWindow& window, float time)
{
    float winWidth = window.getSize().x;
    float winHeight = window.getSize().y;
    float squareSize = 40.f;
    sf::Color shapeColor = sf::Color(0, 0, 0, 150);
    sf::Color squareColor = sf::Color(150, 150, 150, 200);

    sf::RectangleShape shape(sf::Vector2f(winWidth, winHeight));
    sf::RectangleShape square(sf::Vector2f(squareSize, squareSize));
    sf::Text text;

    shape.setFillColor(shapeColor);
    square.setFillColor(squareColor);
    square.setOrigin(squareSize / 2.f, squareSize / 2.f);
    square.setPosition(winWidth / 2.f, winHeight / 2.f);
    square.rotate(time * 100);
    text.setFont(g_font);
    text.setString("processing algorithm...");
    text.setCharacterSize(30);
    text.setOrigin(
            text.getLocalBounds().width / 2.f, 
            text.getLocalBounds().height / 2.f + 100.f
            );
    text.setPosition(winWidth / 2.f, winHeight / 2.f);

    window.pushGLStates();
    window.draw(shape);
    window.draw(square);
    window.draw(text);
    window.popGLStates();
}



std::shared_ptr<Robot> newRobot(Algorithm& algo)
{
    std::shared_ptr<Robot> robot(new Robot());

    robot->load(getResource(SSIM_MODEL));
    robot->setPolicy(model2Policy(algo.bestPolicy()));
    // robot->setPolicy(model2Policy(std::vector<float>(16, 1.f)));

    return robot;
}



void searchThread(Algorithm& algo, unsigned int iterations)
{
    algo.run(iterations);
    g_lock = true;
}



int main(int argc, char** argv)
{
    std::shared_ptr<Robot> robot;
    std::shared_ptr<std::thread> thread(nullptr);
    GradientR gradientr(&fitness, 20, std::vector<float>(16, 0.1f), 1.f);
    float time = -1.f;

    initResourcePath();
    robot = newRobot(gradientr);
    g_font.loadFromFile(getResource("runescape.ttf"));

    OpenGLWindow window(800, 600, "ssim");
    window.init();

    while (window.isOpen())
    {
        float delta = window.deltaTime();

        time -= delta;

        if (time < 0.f && thread.get() == nullptr)
        {
            g_lock = false;

            thread.reset(
                    new std::thread(&searchThread, std::ref(gradientr), 5)
                    );
        }

        if (thread.get() != nullptr && g_lock)
        {
            thread->join();
            thread.reset();
            time = 10.f;
            robot = newRobot(gradientr);
        }

        if (thread.get() == nullptr)
        {
            robot->stepSimulation(delta);
        }

        window.processEvents();

        btVector3 center = robot->bodies().begin()->second->centerOfMass();

        window.setupCam(center[0], center[1], center[2]);
        window.drawMark();
        window.drawGround(100.f);
        window.drawRobot(*robot);

        if (thread.get() != nullptr)
        {
            drawWaitingFrame(window, time);
        }
        else
        {
            drawStats(window, gradientr);
        }

        window.display();

    }

    return 0;
}
