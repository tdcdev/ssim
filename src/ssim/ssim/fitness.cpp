#include <ssim/ssim/fitness.hpp>
#include <ssim/engine/Robot.hpp>
#include <ssim/engine/RigidBody.hpp>
#include <ssim/ssim/tools.hpp>
#include <iostream>



std::vector<float> model2Policy(const std::vector<float>& tmp)
{
    std::vector<float> policy;

    for (unsigned int i = 0; i <= tmp.size(); i++)
    {
        if (i > 0 && i % 4 == 0)
        {
            for (int j = 0; j < 4; j++)
            {
                policy.push_back(tmp[i - 4 + j]);
            }
        }

        if (i < tmp.size())
        {
            policy.push_back(tmp[i]);
        }
    }

    return policy;
}



float fitness(const std::vector<float>& tmp)
{
    Robot robot;
    std::vector<btVector3> positions;
    float remaining = SSIM_FITNESS_SIMULATION_TIME;
    // float my = 0.f;
    float mz = 0.f;

    robot.load(getResource(SSIM_MODEL));
    robot.setPolicy(model2Policy(tmp));

    while (remaining > 0.f)
    {
        robot.stepSimulation(1.f);
        positions.push_back(robot.bodies().begin()->second->centerOfMass());
        remaining -= 1.f;
    }

    for (
        std::vector<btVector3>::const_iterator it = positions.begin();
        it != positions.end();
        it++
        )
    {
        // my += fabs(it->y());
        mz -= fabs(it->z());
    }

    // my /= (float)positions.size();
    mz /= (float)positions.size();

    return positions.back().x() + mz;
}
