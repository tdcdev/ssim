#include <ssim/algorithm/GradientR.hpp>
#include <ssim/ssim/tools.hpp>
#include <algorithm>
#include <iostream>



GradientR::GradientR(
                float (*fitnessFun)(const std::vector<float>&),
                unsigned int nbPop,
                const std::vector<float>& epsilon,
                float scalar
        ):
    Algorithm(epsilon.size(), fitnessFun),
    m_nbPop(nbPop),
    m_epsilon(epsilon),
    m_scalar(scalar)
{
    
}



GradientR::~GradientR()
{

}



void GradientR::init()
{

}



void GradientR::iterationImpl(std::vector<float>& policy)
{
    unsigned int nbParams = policy.size();
    std::vector< std::vector<float> > pop(m_nbPop, policy);
    std::vector< std::vector<int> > delta;
    std::vector< std::vector< std::vector<float> > > scores;
    std::vector<float> gradient;

    makeDelta(delta);
    scores.assign(nbParams, std::vector< std::vector<float> >(3));

    for (unsigned int i = 0; i < m_nbPop; i++)
    {
        float score;

        this->mutate(pop[i], delta[i]);
        score = this->callFitness(pop[i]);

        for (unsigned int j = 0; j < nbParams; j++)
        {
            scores[j][delta[i][j] + 1].push_back(score);
        }
    }

    for (unsigned int i = 0; i < nbParams; i++)
    {
        float g[3] = {
            mean(scores[i][0]),
            mean(scores[i][1]),
            mean(scores[i][2])
        };

        if (g[1] > g[0] && g[1] > g[2])
        {
            gradient.push_back(0.f);
        }
        else
        {
            if (std::fabs(g[2] - g[0]) > std::numeric_limits<float>::epsilon())
            {
                gradient.push_back(g[2] - g[0]);
            }
            else
            {
                if (rand(0, 1) % 2 == 0)
                {
                    gradient.push_back(g[2] - g[1]);
                }
                else
                {
                    gradient.push_back(g[1] - g[0]);
                }
            }
        }
    }

    normalize(gradient);

    for (unsigned int i = 0; i < nbParams; i++)
    {
        policy[i] += gradient[i] * m_scalar;
    }
}



void GradientR::makeDelta(std::vector< std::vector<int> >& delta)
{
    unsigned int nbParams = this->policy().size();
    std::vector< std::vector<int> > tmp(
            nbParams, 
            std::vector<int>(m_nbPop, -1)
            );

    delta.clear();
    delta.assign(m_nbPop, std::vector<int>(nbParams));

    for (unsigned int i = 0; i < nbParams; i++)
    {
        for (unsigned int j = 0; j < m_nbPop; j++)
        {
            tmp[i][j] += j % 3;
        }

        shuffleIntVector(tmp[i]);

        for (unsigned int j = 0; j < m_nbPop; j++)
        {
            delta[j][i] = tmp[i][j];
        }
    }
}



void GradientR::mutate(std::vector<float>& policy, const std::vector<int>& delta)
{
    int n = 0;

    for (
            std::vector<float>::iterator i = policy.begin();
            i != policy.end();
            i++, n++
        )
    {
        *i += (float)delta[n] * m_epsilon[n];
    }
}
