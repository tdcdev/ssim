#include <ssim/algorithm/Annealing.hpp>
#include <ssim/ssim/tools.hpp>
#include <iostream>
#include <cmath>



Annealing::Annealing(
        float (*fitnessFun)(const std::vector<float>&),
        float maxTemp,
        float minTemp,
        const std::vector<float>& epsilon
        ):
    Algorithm(epsilon.size(), fitnessFun),
    m_temp(maxTemp),
    m_maxTemp(maxTemp),
    m_minTemp(minTemp),
    m_epsilon(epsilon)  
{

}



Annealing::~Annealing()
{

}



void Annealing::init()
{
    m_score = this->callFitness(this->policy());
    m_temp = m_maxTemp;
}



void Annealing::iterationImpl(std::vector<float>& policy)
{
    std::vector<float> newPolicy(policy);
    float newScore, delta = m_score;
    float lastTemp = m_temp;

    if (m_temp <= 0 || m_temp <= m_minTemp)
    {
        std::cerr << "Temp must be > minTemp > 0" << std::endl;
        return;
    }

    m_temp *= 0.99;
    this->mutate(newPolicy);
    newScore = this->callFitness(newPolicy);
    delta -= newScore;

    if (delta > 0.f) 
    {
        double p = exp((double)(-delta / lastTemp));

        if (randReal() >= p)
        {
            return;
        }
    }

    policy = newPolicy;
    m_score = newScore;
}



void Annealing::mutate(std::vector<float>& policy)
{
    float delta[3] = {-1.f, 0.f, 1.f};
    int n = 0;

    for (
            std::vector<float>::iterator i = policy.begin();
            i != policy.end();
            i++, n++
        )
    {
        *i += delta[rand(0, 3)] * m_epsilon[n];
    }
}
