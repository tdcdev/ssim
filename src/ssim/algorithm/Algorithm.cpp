#include <ssim/algorithm/Algorithm.hpp>
#include <iostream>



Algorithm::Algorithm(
        unsigned int dimension, 
        float (*fitnessFun)(const std::vector<float>&)
        ):
    m_policy(dimension),
    m_bestPolicy(dimension),
    m_fitness(0.f),
    m_bestFitness(0.f),
    m_fitnessFun(fitnessFun),
    m_iterationCalls(0),
    m_fitnessCalls(0)
{

}



Algorithm::~Algorithm()
{

}



const std::vector<float>& Algorithm::policy() const
{
    return m_policy;
}



const std::vector<float>& Algorithm::bestPolicy() const
{
    return m_bestPolicy;
}



const std::vector< std::vector<float> >& Algorithm::policyEvolution() const
{
    return m_policyEvolution;
}



float Algorithm::fitness() const
{
    return m_fitness;
}



float Algorithm::bestFitness() const
{
    return m_bestFitness;
}



const std::vector<float>& Algorithm::fitnessEvolution() const
{
    return m_fitnessEvolution;
}



unsigned int Algorithm::iterationCalls() const
{
    return m_iterationCalls;
}



unsigned int Algorithm::fitnessCalls() const
{
    return m_fitnessCalls;
}



void Algorithm::setStartingPoint(const std::vector<float>& policy)
{
    if (policy.size() != m_policy.size())
    {
        std::cerr << "Algorithm: policy size error" << std::endl;

        return;
    }

    m_policy = policy;
    m_bestPolicy = m_policy;
    m_fitness = m_fitnessFun(m_policy);
    m_bestFitness = m_fitness;
    m_iterationCalls = 0;
    m_fitnessCalls = 0;

    m_policyEvolution.clear();
    m_fitnessEvolution.clear();

    this->init();
}



void Algorithm::iteration()
{
    std::vector<float> policy(m_policy);
    unsigned int fitnessCalls = m_fitnessCalls;

    this->iterationImpl(policy);

    if (policy.size() != m_policy.size())
    {
        m_fitnessCalls = fitnessCalls;
        std::cerr << "Algorithm: policy size error" << std::endl;

        return;
    }

    m_policy = policy;
    m_fitness = m_fitnessFun(m_policy);
    m_iterationCalls++;

    m_policyEvolution.push_back(m_policy);
    m_fitnessEvolution.push_back(m_fitness);

    if (m_fitness > m_bestFitness)
    {
        m_bestPolicy = m_policy;
        m_bestFitness = m_fitness;
    }
}



void Algorithm::run(unsigned int steps)
{
    for (unsigned int i = 0; i < steps; i++)
    {
        this->iteration();
    }
}



float Algorithm::callFitness(const std::vector<float>& policy)
{
    m_fitnessCalls++;

    return m_fitnessFun(policy);
}
