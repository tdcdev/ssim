#include <ssim/algorithm/HillC.hpp>
#include <ssim/ssim/tools.hpp>



HillC::HillC(
        float (*fitnessFun)(const std::vector<float>&),
        unsigned int nbPop,
        const std::vector<float>& epsilon
        ):
    Algorithm(epsilon.size(), fitnessFun),
    m_nbPop(nbPop),
    m_epsilon(epsilon)  
{

}



HillC::~HillC()
{

}



void HillC::init()
{
    m_score = this->callFitness(this->policy());
}



void HillC::iterationImpl(std::vector<float>& policy)
{
    std::vector< std::vector<float> > pop;
    std::vector<float> scores;
    std::vector<float> prob;
    float sumDiff = 0.f;

    for (unsigned int i = 0; i < m_nbPop; i++)
    {
        std::vector<float> newPolicy(policy);
        this->mutate(newPolicy);

        float score = this->callFitness(newPolicy);
        float diff = score - m_score;
        if (diff > 0.f)
        {	  
            prob.push_back(diff);
            sumDiff += diff;

            pop.push_back(newPolicy);
            scores.push_back(score);
        }
    }

    for (unsigned int i = 0; i < pop.size(); i++) 
    {
        if (randReal() < (double)(scores[i] / sumDiff))
        {
            policy = pop[i];
            m_score = scores[i];
            return;
        }
    }      
}



void HillC::mutate(std::vector<float>& policy)
{
    float delta[3] = {-1.f, 0.f, 1.f};
    int n = 0;

    for (
            std::vector<float>::iterator i = policy.begin();
            i != policy.end();
            i++, n++
        )
    {
        *i += delta[rand(0, 3)] * m_epsilon[n];
    }
}
