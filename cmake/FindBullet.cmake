# Try to find Bullet library
# Once done this will define :
#     BULLET_FOUND - if system found Bullet library
#     BULLET_INCLUDE_DIR - The Bullet include directories
#     BULLET_LIBRARIES - The libraries needed to use Bullet

set(
    BULLET_SEARCH_PATHS
    ${BULLET_ROOT_DIR}
    $ENV{BULLET_ROOT}
    /usr/
    /usr/local
    /opt/
    /opt/local/
    )

find_path(
    BULLET_TMP_INCLUDE_DIR
    btBulletCollisionCommon.h
    PATH_SUFFIXES
    include/bullet
    PATHS
    ${BULLET_SEARCH_PATHS}
    ${BULLET_INCLUDE_DIR}
    )

mark_as_advanced(BULLET_TMP_INCLUDE_DIR)

if(NOT BULLET_TMP_INCLUDE_DIR)
    message(WARNING "Bullet library: could not found include directory")
else()
    set(BULLET_FOUND TRUE)
    set(BULLET_INCLUDE_DIR ${BULLET_TMP_INCLUDE_DIR})
endif()

foreach(COMPONENT ${Bullet_FIND_COMPONENTS})
    message(STATUS "Looking for ${COMPONENT}")
    find_library(
        BULLET_LIBRARY_${COMPONENT}
        ${COMPONENT}
        PATH_SUFFIXES
        lib
        PATHS
        ${BULLET_SEARCH_PATHS}
        ${BULLET_LIBRARIES_DIR}
        )
    mark_as_advanced(BULLET_LIBRARY_${COMPONENT})
    if(NOT BULLET_LIBRARY_${COMPONENT})
        message(STATUS "Looking for ${COMPONENT} - not found")
        set(BULLET_FOUND FALSE)
        set(BULLET_MISSING "${BULLET_MISSING} ${COMPONENT}")
    else()
        message(STATUS "Looking for ${COMPONENT} - found")
        set(BULLET_LIBRARIES ${BULLET_LIBRARIES} ${BULLET_LIBRARY_${COMPONENT}})
    endif()
endforeach()

if(BULLET_MISSING)
    message(WARNING "Bullet library: missing ${BULLET_MISSING}")
endif()

if(NOT BULLET_FOUND)
    if(Bullet_FIND_REQUIRED)
        message(FATAL_ERROR "Bullet library: FATAL ERROR")
    endif()
endif()

if(BULLET_FOUND)
    message(STATUS "Found Bullet in ${BULLET_INCLUDE_DIR}")
endif()
