#ifndef _HINGE_HPP_
#define _HINGE_HPP_



#include <ssim/engine/ObjectComponent.hpp>
#include <LinearMath/btVector3.h>
#include <memory>



class btHingeConstraint;
class RigidBody;



class Hinge: public ObjectComponent
{

    public:

        Hinge(
                const std::string& id,
                std::shared_ptr<RigidBody> rigidBodyA,
                std::shared_ptr<RigidBody> rigidBodyB,
                btVector3 pivA,
                btVector3 pivB,
                btVector3 axis,
                float min,
                float max
             );
        virtual ~Hinge();
        float min() const;
        float max() const;
        float angle() const;
        void setSpeed(float speed, float impulse = 2.f);
        virtual void addToWorld(btDiscreteDynamicsWorld* world);
        virtual void removeFromWorld(btDiscreteDynamicsWorld* world);

    private:

        std::shared_ptr<RigidBody> m_rigidBodyA;
        std::shared_ptr<RigidBody> m_rigidBodyB;
        std::shared_ptr<btHingeConstraint> m_hingeConstraint;

};



#endif
