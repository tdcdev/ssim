#ifndef _OBJECT_HPP_
#define _OBJECT_HPP_



#include <memory>



class btBroadphaseInterface;
class btDefaultCollisionConfiguration;
class btCollisionDispatcher;
class btSequentialImpulseConstraintSolver;
class btDiscreteDynamicsWorld;
class btCollisionShape;
class btDefaultMotionState;
class btRigidBody;



class Object
{

    public:

        Object();
        virtual ~Object();
        void stepSimulation(float seconds);

    protected:

        virtual void simulate(float seconds) = 0;

        std::shared_ptr<btDiscreteDynamicsWorld> m_world;

    private:

        std::shared_ptr<btBroadphaseInterface> m_broadphase;
        std::shared_ptr<btDefaultCollisionConfiguration> m_colConfig;
        std::shared_ptr<btCollisionDispatcher> m_dispatcher;
        std::shared_ptr<btSequentialImpulseConstraintSolver> m_solver;
        std::shared_ptr<btCollisionShape> m_groundShape;
        std::shared_ptr<btDefaultMotionState> m_groundMotionState;
        std::shared_ptr<btRigidBody> m_groundRigidBody;
        float m_time;

};



#endif
