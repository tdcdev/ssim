#ifndef _RIGID_BODY_HPP_
#define _RIGID_BODY_HPP_



#include <string>
#include <memory>
#include <LinearMath/btVector3.h>
#include <ssim/engine/Hinge.hpp>



class btBoxShape;
class btDefaultMotionState;
class btRigidBody;
class btDiscreteDynamicsWorld;



class RigidBody
{

    friend Hinge::Hinge(
                const std::string& id,
                std::shared_ptr<RigidBody> rigidBodyA,
                std::shared_ptr<RigidBody> rigidBodyB,
                btVector3 pivA,
                btVector3 pivB,
                btVector3 axis,
                float min,
                float max
             );

    public:

        RigidBody(
                const std::string& id,
                btVector3 size,
                btVector3 origin,
                float mass
                );
        virtual ~RigidBody();
        const std::string& id() const;
        btVector3 size() const;
        btVector3 origin() const;
        float mass() const;
        btVector3 centerOfMass() const;
        void getMatrix(float* matrix) const;
        void activate();
        virtual void addToWorld(btDiscreteDynamicsWorld* world);
        virtual void removeFromWorld(btDiscreteDynamicsWorld* world);

    private:

        std::string m_id;
        btVector3 m_size;
        btVector3 m_origin;
        float m_mass;
        std::shared_ptr<btBoxShape> m_shape;
        std::shared_ptr<btDefaultMotionState> m_mostionState;
        std::shared_ptr<btRigidBody> m_rigidBody;

};



#endif
