#ifndef _ROBOT_HPP_
#define _ROBOT_HPP_



#include <ssim/engine/Object.hpp>
#include <vector>
#include <map>



class RigidBody;
class Hinge;



typedef std::map< std::string, std::shared_ptr<RigidBody> > RigidBodyMap;
typedef std::map< std::string, std::shared_ptr<Hinge> > HingeMap;



class Robot: public Object
{

    public:

        Robot();
        virtual ~Robot();
        const RigidBodyMap& bodies() const;
        const HingeMap& hinges() const;
        const std::shared_ptr<RigidBody> body(const std::string& id) const;
        const std::shared_ptr<Hinge> hinge(const std::string& id) const;
        const std::vector<float>& policy() const;
        bool load(const std::string& file);
        bool setPolicy(const std::vector<float>& policy);

    protected:

        virtual void simulate(float seconds);

    private:

        RigidBodyMap m_bodies;
        HingeMap m_hinges;
        std::vector<float> m_policy;
        float m_progress;

};



#endif
