#ifndef _OBJECT_COMPONENT_HPP_
#define _OBJECT_COMPONENT_HPP_



#include <string>



class btDiscreteDynamicsWorld;



class ObjectComponent
{

    public:

        ObjectComponent(const std::string& id);
        virtual ~ObjectComponent();
        const std::string& id() const;
        virtual void addToWorld(btDiscreteDynamicsWorld* world) = 0;
        virtual void removeFromWorld(btDiscreteDynamicsWorld* world) = 0;

    protected:

        const std::string m_id;

};



#endif
