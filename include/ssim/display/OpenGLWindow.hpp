#ifndef _OPEN_GL_WINDOW_HPP_
#define _OPEN_GL_WINDOW_HPP_



#include <SFML/OpenGL.hpp>
#include <SFML/Graphics.hpp>



class RigidBody;
class Robot;



class OpenGLWindow: public sf::RenderWindow
{

    public:

        OpenGLWindow(
                unsigned int w,
                unsigned int h,
                const std::string& name
                );
        virtual ~OpenGLWindow();
        float deltaTime() const;
        void init();
        void processEvents();
        void setupCam(float x, float y, float z);
        void enableLighting(bool on);
        void enableTextures(bool on);
        void drawMark();
        void drawGround(float side);
        void drawCube(float x, float y, float z);
        void drawRigidBody(const RigidBody& body);
        void drawRobot(const Robot& robot);

    private:

        sf::ContextSettings generateContextSettings();
        void processCamEvents();

        unsigned int m_width;
        unsigned int m_height;
        sf::Clock m_clock;
        float m_deltaTime;
        float m_camForward[4];
        float m_camLeft[4];
        float m_camTarget[4];
        float m_camDistance;
        float m_camSpeed;
        sf::Texture m_texGround;
        sf::Texture m_texCube;

};



#endif
