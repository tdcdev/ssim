#ifndef _TOOLS_HPP_
#define _TOOLS_HPP_



#include <string>
#include <vector>



bool initResourcePath();
std::string getResource(const std::string& res);
int rand(int min, int max);
double randReal();
void shuffleIntVector(std::vector<int>& v);
void rotate(float* res, float* v, float* axis, float rad);
void rotate(float* v, float* axis, float rad);
float norm(const float* v);
void normalize(float* v);
void normalize(std::vector<float>& v);
float mean(const std::vector<float>& v);



#endif
