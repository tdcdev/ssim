#ifndef _FITNESS_HPP_
#define _FITNESS_HPP_



#define SSIM_MODEL "model2.xml"
#define SSIM_FITNESS_SIMULATION_TIME 10.f



#include <vector>



std::vector<float> model2Policy(const std::vector<float>& tmp);
float fitness(const std::vector<float>& tmp);



#endif
