#ifndef _GRADIENT_R_HPP_
#define _GRADIENT_R_HPP_



#include <ssim/algorithm/Algorithm.hpp>



class GradientR: public Algorithm
{

    public:

        GradientR(
                float (*fitnessFun)(const std::vector<float>&),
                unsigned int nbPop,
                const std::vector<float>& epsilon,
                float scalar
                );
        virtual ~GradientR();

    protected:

        virtual void init();
        virtual void iterationImpl(std::vector<float>& policy);

    private:

        void makeDelta(std::vector< std::vector<int> >& delta);
        void mutate(std::vector<float>& policy, const std::vector<int>& delta);

        unsigned int m_nbPop;
        std::vector<float> m_epsilon;
        float m_scalar;

};



#endif
