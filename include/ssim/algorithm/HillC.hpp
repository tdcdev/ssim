#ifndef _HILL_C_HPP_
#define _HILL_C_HPP_



#include <ssim/algorithm/Algorithm.hpp>



class HillC: public Algorithm
{

    public:

        HillC(
                float (*fitnessFun)(const std::vector<float>&),
                unsigned int nbPop,
                const std::vector<float>& epsilon
             );
        virtual ~HillC();

    protected:

        virtual void init();
        virtual void iterationImpl(std::vector<float>& policy);

    private:

        void mutate(std::vector<float>& policy);

        float m_score;
        unsigned int m_nbPop;
        std::vector<float> m_epsilon;

};



#endif
