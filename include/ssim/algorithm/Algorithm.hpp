#ifndef _ALGORITHM_HPP_
#define _ALGORITHM_HPP_



#include <vector>



class Algorithm
{

    public:

        Algorithm(
                unsigned int dimension, 
                float (*fitness)(const std::vector<float>&)
                );
        virtual ~Algorithm();
        const std::vector<float>& policy() const;
        const std::vector<float>& bestPolicy() const;
        const std::vector< std::vector<float> >& policyEvolution() const;
        float fitness() const;
        float bestFitness() const;
        const std::vector<float>& fitnessEvolution() const;
        unsigned int iterationCalls() const;
        unsigned int fitnessCalls() const;
        void setStartingPoint(const std::vector<float>& policy);
        void iteration();
        void run(unsigned int steps);

    protected:

        float callFitness(const std::vector<float>& policy);
        virtual void init() = 0;
        virtual void iterationImpl(std::vector<float>& policy) = 0;

    private:

        std::vector<float> m_policy;
        std::vector<float> m_bestPolicy;
        std::vector< std::vector<float> >m_policyEvolution;
        float m_fitness;
        float m_bestFitness;
        std::vector<float> m_fitnessEvolution;
        float (*m_fitnessFun)(const std::vector<float>&);
        unsigned int m_iterationCalls;
        unsigned int m_fitnessCalls;

};



#endif
