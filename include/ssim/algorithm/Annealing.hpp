#ifndef _ANNEALING_HPP_
#define _ANNEALING_HPP_



#include <ssim/algorithm/Algorithm.hpp>



class Annealing: public Algorithm
{

    public:

        Annealing(
                float (*fitnessFun)(const std::vector<float>&),
                float maxTemp,
                float minTemp,
                const std::vector<float>& epsilon
                );
        virtual ~Annealing();

    protected:

        virtual void init();
        virtual void iterationImpl(std::vector<float>& policy);

    private:

        void mutate(std::vector<float>& policy);

        float m_score;
        float m_temp;
        float m_maxTemp;
        float m_minTemp;
        std::vector<float> m_epsilon;

};



#endif
